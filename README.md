MicroC Compiler
===============

MSU - CSE 450 - Project 4
-------------------------

### Authors:
+ Eric Miller (mill2121)
+ Domas Siaudvytis (siaudvyt)
+ Zack Taylor (taylorz3)
+ Grayson Wright (wrightg7)

###  Description
A compiler that will translate a C-like language called microC into x86 assembly code.  
Can be as many functions with as many parameters as you want.  
If functions or variables are not declared, you will get an error message accordingly.  
Precedence is in place for all operations and negative numbers are in place.  
Int is the only data type available for use.  
Write displays variables and constants.  
Variables and arguments are scoped within their appropriate functions.  
Comments are in place.  
Functions calls and return statement all work.

### Restrictions

All variable declarations must be in the top of a function  
No return statements within a conditionals

### Conditionals

If statements and while loops are in effect  
All if statements and while loops must end with a semicolon: `;`

### Optimizations in effect

Constant Folding and Algebraic Simplication

- If a variable can be turned into a constant, then it is: 
	- Ex. `x = 3; y = x;` --> `y = 3;`

- If a constants can be folded, they are:
	- Ex. `x = 3 + 3;` --> `x = 6;`

- Identities are deleted
	- Ex. `x = x + 0` --> Deleted
	- Ex. `x = x * 1` --> Deleted
 	- Ex. `x = x - 1` --> Deleted
	- Ex. `x = x`     --> Deleted

[Class Project Page][projDescription]

### DOT Output

In order to display DOT graphs of code being processed, use the `@DOT` directive before the statement  
or function declaration you wish to dissect. The format of this command is:

	@DOT <name> <code>

Here, `<code>` is the code you want to print a DOT graph of.  
`<name>` is the file location where the output will be placed. The filename cannot contain  
a full path, but will be placed inside the current working directory.  
The `<name>` parameter will be parameter will have the string `.dot` appended onto it before being  
written to file.

Additionally, a second file will be generated in the same directory, named `<name>-optimized.dot`.  
This file shows the results of optimization on the program.

#### Note:

Optimizations are more effective, and therefore easier to see, when the @DOT directive  
is placed at the function declaration level (right before a function declaration.)

In order to process and display the DOT graphs, a simple script called "processDot" has been provided.  
Simply type from a command prompt: `./processDot <name>`, and PNG files for both the normal and optimized  
graphs will be produced.

### How to Run
command "make" will build the compiler and run all of our test files for you.




[projDescription]: http://www.cse.msu.edu/~cse450/Projects/Project4/project4.html
