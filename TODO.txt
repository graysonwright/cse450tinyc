General:
	Create better (smaller) test cases, fix makefile and directory structure to make them more useable.

Optimizations:
	Algebraic simplification
		- Parts completed:
			1) x + 0 => x
			2) x * 1 => x
			3) x * 0 => 0
			4) x = x => Deleted (Therefore statement x = x + 0 will be turned first into x = x due to step 1, and then deleted)

		- To Complete:
			1) if(x<0) => Deleted
			2) If a function declaration has no statements, delete it
			3) If a variable is never referenced in a functions' statements, delete the variable's declaration

	Advanced:
		Inline functions?
		Dead code elimination?

