#ifndef ASSIGNMENT_H
#define ASSIGNMENT_H

#include "Statement.h"
#include "Variables.h"
#include <iostream>

using namespace std;

class CAssignmentOperator : public CStatement
{
private:
	CVariable *m_left;
	CExpression *m_right;

	CContext *m_context;

public:
	CAssignmentOperator(CVariable *lexp, CExpression *right): m_left(lexp), m_right(right)
	{
		m_right->SetParent(this);
	}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitAssignmentOperator(this);
	}

	CVariable *GetLeft() { return m_left; }
	CExpression *GetRight() { return m_right; }

	virtual void Swap(CNode *oldChild, CNode *newChild)
	{
		assert( m_right == oldChild);
		delete m_right;
		m_right = (CExpression *) newChild;
	}
};

class CReturnStatement: public CStatement
{
private:
	CExpression *m_ReturnValue;

public: 
	CReturnStatement(CExpression *exp): m_ReturnValue(exp) { m_ReturnValue->SetParent(this); }


	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitReturnStatement(this);
	}

	CExpression* GetChild() { return m_ReturnValue; }

	virtual void Swap(CNode *oldChild, CNode *newChild)
	{
		if ( m_ReturnValue == oldChild)
		{
			delete m_ReturnValue;
			m_ReturnValue = (CExpression *) newChild;
		}
	}
};


#endif
