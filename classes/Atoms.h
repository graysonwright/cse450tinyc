#ifndef ATOMS_H
#define ATOMS_H

#include "Expression.h"
#include <sstream>

// For numbers (constants)
class CNumber : public CExpression
{
protected:
	int m_val;

public:
	CNumber (int val): m_val (val) {}

	void setValue(int val) { m_val = val; }
	int getValue () { return m_val; }

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitNumber(this);
	}

	virtual void Swap( CNode *oldChild, CNode *newChild) {}

};

class CEmptyStatement : public CStatement
{
public:
	virtual void Swap(CNode *oldNode, CNode *newNode) {}
	virtual void AcceptVisitor(CNodeVisitor *v) { v->VisitEmptyStatement(this); }
};

#endif
