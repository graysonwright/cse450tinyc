#ifndef BINARY_OPERATORS_H
#define BINARY_OPERATORS_H

#include "Expression.h"
#include <cmath>
#include <sstream>
#include <cassert>

/* This file contains classes that represent binary expressions.
 * Add, Subtract, Multiply, Divide, Exponent, GreaterThan, and LessThan operations are defined in this file
 * Each operation has two child expressions, which are evaluated before the parent expression.
 *
 * The CBinary operator class is abstract, and only defines the two children that are common between all binary operation classes
 * Cuts down on repeated code.
 */

class CBinaryOperator : public CExpression
{
protected:
	CExpression *m_left, *m_right, *m_parent;

	CBinaryOperator () {}
	CBinaryOperator (CExpression *left, CExpression *right): m_left (left), m_right (right)
        {
                m_parent = NULL;
                m_left->SetParent(this);
                m_right->SetParent(this);
        }

public:
        void setLeftExp(CExpression *left) { m_left = left; m_left->SetParent(this); }
        void setRightExp(CExpression *right) { m_right = right; m_right->SetParent(this); }
        void setChildren(CExpression *left, CExpression *right)
        {
                m_left = left;
                m_right = right;
                m_left->SetParent(this);
                m_right->SetParent(this);
        }

	virtual ~CBinaryOperator ()
	{
		delete m_left;
		delete m_right;
	}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		m_left->AcceptVisitor(v);
		m_right->AcceptVisitor(v);
	}

	virtual CExpression *GetLeft() { return m_left; }
	virtual CExpression *GetRight() { return m_right; }

        virtual void Swap(CNode *oldChild, CNode *newChild)
        {
                if( m_left == oldChild) {
                        delete m_left;
                        m_left = (CExpression *) newChild;
                        m_left->SetParent(this);
                } else if (m_right == oldChild) {
                        delete m_right;
                        m_right = (CExpression *) newChild;
                        m_right->SetParent(this);
                } else assert(false);
        }
};

// For additive expressions
// Parser production:
// exp -> exp '+' prod
class CAddExpression : public CBinaryOperator
{
public:
	CAddExpression () {}
	CAddExpression (CExpression *left, CExpression* right): CBinaryOperator(left, right) {}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitAddExpression(this);
	}
}; 

// For subtractive expressions
// Parser production:
// exp -> exp '-' prod
class CSubExpression : public CBinaryOperator
{
public:
	CSubExpression () {}
	CSubExpression (CExpression *left, CExpression* right): CBinaryOperator(left, right) {}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitSubExpression(this);
	}
}; 

// For multiplicitive expressions
// Parser production:
// prod: prod MUL base
class CMultExpression : public CBinaryOperator
{
public:
	CMultExpression () {}
	CMultExpression (CExpression *left, CExpression *right): CBinaryOperator(left, right) {}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitMultExpression(this);
	}
};

// For division expressions
// Parser production:
// prod: prod DIV base
class CDivExpression : public CBinaryOperator
{
public:
	CDivExpression () {}
	CDivExpression (CExpression *left, CExpression *right): CBinaryOperator(left, right) {}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitDivExpression(this);
	}
};


// For exponential expressions
// Parser production:
// base: base '%' atom
class CModExpression : public CBinaryOperator
{
public:
	CModExpression () {}
	CModExpression (CExpression *left, CExpression *right): CBinaryOperator(left, right) {}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitModExpression(this);
	}
};

// For (less than) comparison expressions
// Parser production:
// exp: exp '<' exp
class CLessThanExpression : public CBinaryOperator
{
public:
	CLessThanExpression () {}
	CLessThanExpression (CExpression *left, CExpression *right): CBinaryOperator(left, right) {}

	virtual void AcceptVisitor( CNodeVisitor *v )
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitLessThanExpression(this);
	}
};

// For (greater than) comparison expressions
// Parser production:
// exp: exp '>' exp
class CGreaterThanExpression : public CBinaryOperator
{
public:
	CGreaterThanExpression () {}
	CGreaterThanExpression (CExpression *left, CExpression *right): CBinaryOperator(left, right) {}

	virtual void AcceptVisitor( CNodeVisitor *v )
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitGreaterThanExpression(this);
	}
};

// For (less than or equal to) comparison expressions
// Parser production:
// exp: exp LESSOR exp
class CLessThanOrEqualToExpression : public CBinaryOperator
{
public:
	CLessThanOrEqualToExpression () {}
	CLessThanOrEqualToExpression (CExpression *left, CExpression *right): CBinaryOperator(left, right) {}

	virtual void AcceptVisitor( CNodeVisitor *v )
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitLessThanOrEqualToExpression(this);
	}
};

// For (greater than or equal to) comparison expressions
// Parser production:
// exp: exp GREATEROR exp
class CGreaterThanOrEqualToExpression : public CBinaryOperator
{
public:
	CGreaterThanOrEqualToExpression () {}
	CGreaterThanOrEqualToExpression (CExpression *left, CExpression *right): CBinaryOperator(left, right) {}

	virtual void AcceptVisitor( CNodeVisitor *v )
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitGreaterThanOrEqualToExpression(this);
	}
};

// For (equivalence) comparison expressions
// Parser production:
// exp: exp EQUALTO exp
class CEqualToExpression : public CBinaryOperator
{
public:
	CEqualToExpression () {}
	CEqualToExpression (CExpression *left, CExpression *right): CBinaryOperator(left, right) {}
	
	virtual void AcceptVisitor( CNodeVisitor *v )
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitEqualToExpression(this);
	}
};

// For (nonequivalence) comparison expressions
// Parser production:
// exp: exp NOTEQUALTO exp
class CNotEqualToExpression : public CBinaryOperator
{
public:
	CNotEqualToExpression () {}
	CNotEqualToExpression (CExpression *left, CExpression *right): CBinaryOperator(left, right) {}
	
	virtual void AcceptVisitor( CNodeVisitor *v )
	{
		CBinaryOperator::AcceptVisitor(v);
		v->VisitNotEqualToExpression(this);
	}
};


#endif
