#ifndef CONDITIONALS_H
#define CONDITIONALS_H

#include "Node.h"
#include <sstream>
#include <cassert>

/*	
 *	Conditionals.h
 *
 *	This file contains classes for if/while/for blocks.
 *	if_block, while_block, and for_block are defiend in this file.
 *
 */

class CConditional : public CStatement
{
protected:
	CExpression *m_condition;
	CStatement *m_block;

	CConditional () {}
	CConditional ( CExpression *cond, CStatementBlock *block ) : m_condition (cond), m_block (block)
	{
		m_condition->SetParent(this);
		m_block->SetParent(this);
	}

public:
	void SetCondition( CExpression *c ) { m_condition = c; }

	//virtual void AcceptVisitor( CNodeVisitor *v) {}
	virtual void Swap( CNode *node1, CNode *node2 )
	{
		if( m_condition == node1 )
		{
			delete m_condition;
			m_condition = (CExpression *) node2;
		}
		if( m_block == node1 )
		{
			delete m_condition;
			m_block = (CStatement *) node2;
		}
	}

	CExpression *GetConditional() { return m_condition; }
	CStatement *GetStatement() { return m_block; }

};

/*
 * CIfBlock
 */
class CIfBlock : public CConditional
{
public:
	CIfBlock( CExpression *cond, CStatementBlock *block ) : CConditional( cond, block ) {}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitIfBlock(this);
	}

};

/*
 * CWhileBlock
 */
class CWhileBlock : public CConditional
{
public:
	CWhileBlock( CExpression *cond, CStatementBlock *block ) : CConditional( cond, block ) {}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitWhileBlock(this);
	}

};

#endif
