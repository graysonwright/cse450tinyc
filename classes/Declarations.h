#ifndef DECLARATIONS_H
#define DECLARATIONS_H

#include "Node.h"
#include "Statement.h"
#include "Variables.h"
#include <string>
#include <vector>
#include <set>

class CDeclaration : public CNode
{
private:
	string m_name;

public:
	string getName() const { return m_name; }
	void setName(string name) { m_name = name; }
};


class CVarDeclaration : public CDeclaration
{
public:
	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitVarDeclaration(this);
	}

	virtual void Swap(CNode *oldChild, CNode *newChild) {}
};

class CParamDeclaration : public CDeclaration
{
public:
	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitParamDeclaration(this);
	}

	virtual void Swap(CNode *oldChild, CNode *newChild) {}

};

class CVarDeclarationList : public vector<CVarDeclaration *>
{
public:
	CVarDeclarationList(CVarDeclaration *dec): vector<CVarDeclaration *>(1, dec) {}
	CVarDeclarationList(): vector<CVarDeclaration *>() {}
};

class CParamDeclarationList : public vector<CParamDeclaration *>
{
public:
	CParamDeclarationList(CParamDeclaration *dec): vector<CParamDeclaration *>(1, dec) {}
	CParamDeclarationList(): vector<CParamDeclaration *>() {}
};


class CParameterList : public vector<CExpression *>
{
public:
	CParameterList(CExpression *exp): vector<CExpression *>(1, exp) {}
	CParameterList(): vector<CExpression *>() {}

	
};



class CFuncDeclaration : public CDeclaration
{
private:
	CParamDeclarationList m_paramDeclarations;
	CVarDeclarationList m_varDeclarations;
	CStatementList m_statements;

public:
	CFuncDeclaration(CParamDeclarationList *paramList, CVarDeclarationList *varDecs, CStatementList *stmtList): 
		m_paramDeclarations(*paramList), m_varDeclarations(*varDecs), m_statements(*stmtList)
	{
		for( CParamDeclarationList::iterator it = m_paramDeclarations.begin(); it != m_paramDeclarations.end(); it++)
			(*it)->SetParent(this);
		for( CVarDeclarationList::iterator it = m_varDeclarations.begin(); it != m_varDeclarations.end(); it++)
			(*it)->SetParent(this);
		for( CStatementList::iterator it = m_statements.begin(); it != m_statements.end(); it++)
			(*it)->SetParent(this);
	}

	CParamDeclarationList &GetParamDeclarations() { return m_paramDeclarations; }
	CVarDeclarationList &GetVarDeclarations() { return m_varDeclarations; }
	CStatementList &GetStatements() { return m_statements; }

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitFuncDeclaration(this);
	}

	
	virtual void Swap(CNode *oldChild, CNode *newChild)
	{
		for (unsigned int i = 0; i < m_paramDeclarations.size(); i++)
			if ( m_paramDeclarations[i] == oldChild)
			{
				delete m_paramDeclarations[i];
				m_paramDeclarations[i] = (CParamDeclaration *) newChild;
				m_paramDeclarations[i]->SetParent(this);
			}
		for (unsigned int i = 0; i < m_varDeclarations.size(); i++)
			if ( m_varDeclarations[i] == oldChild)
			{
				delete m_varDeclarations[i];
				m_varDeclarations[i] = (CVarDeclaration *) newChild;
				m_varDeclarations[i]->SetParent(this);
			}
		for (unsigned int i = 0; i < m_statements.size(); i++)
			if ( m_statements[i] == oldChild)
			{
				delete m_statements[i];
				m_statements[i] = (CStatement *) newChild;
				m_statements[i]->SetParent(this);
			}
	}

};

class CFunctionCall : public CExpression
{

private:
	CParameterList m_ParameterList;
	std::string m_Name;

public:
	CFunctionCall(std::string name, CParameterList *params): m_ParameterList(*params), m_Name(name)
	{
		for( CParameterList::iterator it = m_ParameterList.begin(); it != m_ParameterList.end(); it++)
			(*it)->SetParent(this);
	}

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitFunctionCall(this);
	}

	std::string GetName() { return m_Name; }
	CParameterList &GetParameters() { return m_ParameterList; }

	virtual void Swap(CNode *oldChild, CNode *newChild)
	{
		for(CParameterList::iterator it = m_ParameterList.begin(); it != m_ParameterList.end(); it++)
			if( *it == oldChild) {
				delete *it;
				*it = (CExpression *) newChild;
				(*it)->SetParent(this);
			}
	}
};



#endif
