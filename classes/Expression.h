#ifndef C_EXPRESSION_H
#define C_EXPRESSION_H

#include "Statement.h" 

// Abstract base class for tinyC Expressions
/* An expression is anything that can evaluate to a number.
 * EX: 3+5
 * 	-2
 * 	foo(x, y) // returns a value
 *
 * If an input command consists of only an expression, then it is also a Statement,
 * and can be executed to output "Answer: ..."
 */
class CExpression : public CStatement
{
};

#endif
