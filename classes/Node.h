#ifndef NODE_H
#define NODE_H

#include <string>
#include <iostream>
#include "Visitor.h"

using namespace std;

class CNode
{
private:
	CNode *m_parent;

public:
	void SetParent( CNode *parent ) { m_parent = parent; }
	CNode *GetParent() { return m_parent; }

	virtual void AcceptVisitor( CNodeVisitor *) = 0;
	virtual void Swap( CNode *oldChild, CNode* newChild) =0;
};

#endif
