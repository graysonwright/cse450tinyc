#ifndef OUTPUT_H
#define OUTPUT_H

#include "Statement.h"
#include "Expression.h"

class CWriteStatement : public CStatement
{
private:
	CExpression *m_argument;

public:
	CWriteStatement(CExpression *arg): m_argument(arg) { m_argument->SetParent(this); }

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitWriteStatement(this);
	}

	CExpression *GetChild() { return m_argument; }

	virtual void Swap(CNode *oldChild, CNode *newChild)
	{
		if ( m_argument == oldChild) {
			delete m_argument;
			m_argument = (CExpression *) newChild;
			m_argument->SetParent(this);
		}
	}
};

#endif
