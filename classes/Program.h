#ifndef PROGRAM_H
#define PROGRAM_H

#include "Node.h"
#include "Declarations.h"
#include <vector>

using namespace std;

class CProgram : public CNode
{
protected:
	vector<CDeclaration *> m_declarations;

public:
	void addDeclaration(CDeclaration *dec) { m_declarations.push_back(dec); }

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitProgram(this);
	}

	vector<CDeclaration *> &GetDeclarations() { return m_declarations; }

	virtual void Swap(CNode *oldChild, CNode *newChild)
	{
		for (unsigned int i = 0; i < m_declarations.size(); i++)
			if (m_declarations[i] == oldChild)
			{
				delete m_declarations[i];
				m_declarations[i] = (CDeclaration *) newChild;
				m_declarations[i]->SetParent(this);
			}
	}
};

#endif
