#ifndef C_STATEMENT_H
#define C_STATEMENT_H

#include "Node.h"
#include <vector>
class CContext;
class CExpression;

/* A single command in tinyc.
 */
class CStatement : public CNode
{
};

class CStatementList : public vector<CStatement *>
{
public:
	CStatementList(CStatement *stmt): vector<CStatement *>(1, stmt) {}
};

class CStatementBlock : public CStatement
{
public:
	CStatementBlock(CStatementList *list): m_block(list) {}

	// TODO
	virtual void AcceptVisitor( CNodeVisitor* v) {v->VisitStatementBlock(this);}
	virtual void Swap(CNode *node1, CNode *node2) {}

	CStatementList *GetStatements() { return m_block; }

private:
	CStatementList *m_block;
};

#endif
