#ifndef VARIABLES_H
#define VARIABLES_H

#include "Expression.h"
#include <string>
#include <map>

using namespace std;


class CVariableNotFoundException : public std::exception
{
	virtual const char* what() const throw()
	{
		return "Error. Undefined variable in expression";
	}
};

// possible way to extend context to use block-level scoping:
// Create a pointer to a "parent" context, and if the variable is not found in the current context,
//     search through the parent context.
class CContext
{
protected:
	std::map<std::string, int> m_variableMap;

public:
	CContext() {}

	void addVariable(std::string key, int location) { m_variableMap[key] = location; }
	bool hasVariable(std::string key) { return ( m_variableMap.find(key) != m_variableMap.end() ); }
	int getLocation(std::string key) { if( !hasVariable(key) ) throw *(new CVariableNotFoundException()); return m_variableMap[key]; }
};

class CVariable : public CExpression
{
protected:
	std::string m_name;

public:
	CVariable (std::string name): m_name(name) {}
	CVariable (std::string *name): m_name(*name) {}

	std::string getName() { return m_name; }

	virtual void AcceptVisitor( CNodeVisitor *v)
	{
		v->VisitVariable(this);
	}

	virtual void Swap( CNode *oldChild, CNode *newChild) {}
};

#endif
