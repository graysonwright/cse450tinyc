#ifndef VISITOR_H
#define VISITOR_H

class CProgram;
class CAssignmentOperator;
class CReturnStatement;
class CNumber;
class CVariable;
class CStatementList;
class CWriteStatement;
class CFunctionCall;
class CFuncDeclaration;
class CParameterList;
class CParamDeclaration;
class CVarDeclaration;
class CParamDeclarationList;
class CVarDeclarationList;
class CModExpression;
class CDivExpression;
class CMultExpression;
class CSubExpression;
class CAddExpression;
class CLessThanExpression;
class CGreaterThanExpression;
class CLessThanOrEqualToExpression;
class CGreaterThanOrEqualToExpression;
class CEqualToExpression;
class CNotEqualToExpression;
class CStatementBlock;
class CConditional;
class CIfBlock;
class CWhileBlock;
class CEmptyStatement;

class CNodeVisitor
{
public:
	CNodeVisitor() {}
	virtual ~CNodeVisitor() {}

	virtual void VisitProgram( CProgram *program) =0;
	virtual void VisitAssignmentOperator( CAssignmentOperator *op ) =0;
	virtual void VisitReturnStatement( CReturnStatement *stmt) =0;
	virtual void VisitNumber( CNumber *arg) =0;
	virtual void VisitVariable( CVariable *arg) =0;
	virtual void VisitWriteStatement( CWriteStatement *arg) =0;
	virtual void VisitFunctionCall( CFunctionCall *arg) =0;
	virtual void VisitFuncDeclaration( CFuncDeclaration *arg) =0;
	virtual void VisitParamDeclaration( CParamDeclaration *arg) =0;
	virtual void VisitVarDeclaration( CVarDeclaration *arg) =0;
	virtual void VisitModExpression( CModExpression *arg) =0;
	virtual void VisitDivExpression( CDivExpression *arg) =0;
	virtual void VisitMultExpression( CMultExpression *arg) =0;
	virtual void VisitSubExpression( CSubExpression *arg) =0;
	virtual void VisitAddExpression( CAddExpression *arg) =0;
	virtual void VisitLessThanExpression( CLessThanExpression *arg ) =0;
	virtual void VisitGreaterThanExpression( CGreaterThanExpression *arg ) =0;
	virtual void VisitLessThanOrEqualToExpression( CLessThanOrEqualToExpression *arg ) =0;
	virtual void VisitGreaterThanOrEqualToExpression( CGreaterThanOrEqualToExpression *arg ) =0;
	virtual void VisitEqualToExpression( CEqualToExpression *arg ) =0;
	virtual void VisitNotEqualToExpression( CNotEqualToExpression *arg ) =0;
	virtual void VisitStatementBlock( CStatementBlock *arg ) =0;
	virtual void VisitIfBlock( CIfBlock *arg ) =0;
	virtual void VisitWhileBlock( CWhileBlock *arg ) =0;
	virtual void VisitEmptyStatement( CEmptyStatement *stmt) =0;
};

#endif
