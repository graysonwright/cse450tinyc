#ifndef DOT_VISITOR_H
#define DOT_VISITOR_H

#include "../Visitor.h"
#include <fstream>

class DotVisitor : public CNodeVisitor
{
private:
	std::ofstream out;

public:
	DotVisitor(std::string filename)
	{
		out.open(filename.c_str());
		out << "digraph statement {" << endl;
	}

	~DotVisitor()
	{
		out << "}" << endl;
		out.close();
	}

	virtual void VisitProgram( CProgram *prog)
	{
		vector<CDeclaration *> decs = prog->GetDeclarations();
		for( vector<CDeclaration *>::iterator it = decs.begin(); it != decs.end(); it++)
		{
			(*it)->AcceptVisitor(this);
		}
	}

	virtual void VisitFuncDeclaration( CFuncDeclaration *arg)
	{
		out << "n"<< arg << " [label=\"" << arg->getName() << "\"];" << endl;

		CParamDeclarationList &paramDeclarations = arg->GetParamDeclarations();
		CVarDeclarationList &varDeclarations = arg->GetVarDeclarations();
		CStatementList &statements = arg->GetStatements();

		vector<CParamDeclaration *>::iterator itParam;
		vector<CVarDeclaration *>::iterator itVar;
		vector<CStatement *>::iterator itStatement;

		for(itParam = paramDeclarations.begin(); itParam != paramDeclarations.end(); itParam++)
		{
			(*itParam)->AcceptVisitor(this);
			out << "n" << arg << " -> n" << (*itParam) << " [style=dotted];" << endl;
		}

		for(itVar = varDeclarations.begin(); itVar != varDeclarations.end(); itVar++)
		{
			(*itVar)->AcceptVisitor(this);
			out << "n" << arg << " -> n" << (*itVar) << ";" << endl;
		}

		for(itStatement = statements.begin(); itStatement != statements.end(); itStatement++)
		{
			(*itStatement)->AcceptVisitor(this);
			out << "n" << arg << " -> n" << (*itStatement) << ";" << endl;
		}

	}

	virtual void VisitParamDeclaration( CParamDeclaration *arg)
	{
		out << "n" << arg << " [label=\"int " << arg->getName() << "\"];" << endl;
	}

	virtual void VisitVarDeclaration( CVarDeclaration *arg)
	{
		out << "n" << arg << " [label=\"int " << arg->getName() << "\"];" << endl;
	}

	/* Used. */
	virtual void VisitAssignmentOperator( CAssignmentOperator *op )
	{
		out << "n" << op << " [label=\"=\"];" << endl;
		op->GetLeft()->AcceptVisitor(this);
		op->GetRight()->AcceptVisitor(this);

		out << "n" << op << " -> n" << op->GetLeft() << endl;
		out << "n" << op << " -> n" << op->GetRight() << endl;
	}
	
	virtual void VisitReturnStatement( CReturnStatement *stmt)
	{
		out << "n" << stmt << " [label=\"return\"];" << endl;
		stmt->GetChild()->AcceptVisitor(this);

		out << "n" << stmt << " -> n" << stmt->GetChild() << endl;
	}

	virtual void VisitNumber( CNumber *arg)
	{
		out << "n" << arg << " [label=\"" << arg->getValue() << "\"];" << endl;
	}

	virtual void VisitVariable( CVariable *arg)
	{
		out << "n" << arg << " [label=\"" << arg->getName() << "\"];" << endl;
	}

	virtual void VisitWriteStatement( CWriteStatement *arg)
	{
		out << "n" << arg << " [label=\"write\"];" << endl;
		arg->GetChild()->AcceptVisitor(this);

		out << "n" << arg << " -> n" << arg->GetChild() << endl;
	}

	virtual void VisitFunctionCall( CFunctionCall *arg)
	{
		out << "n" << arg << " [label=\"" << arg->GetName() << "\"];" << endl;

		CParameterList &parameters = arg->GetParameters();
		for(CParameterList::iterator it = parameters.begin(); it != parameters.end(); it++)
			(*it)->AcceptVisitor(this);

		for(CParameterList::iterator it = parameters.begin(); it != parameters.end(); it++)
			out << "n" << arg << " -> n" << (*it) << endl;
	}

	virtual void VisitModExpression( CModExpression *arg)
	{
		out << "n" << arg << " [label=\"%\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << endl;
	}

	virtual void VisitDivExpression( CDivExpression *arg)
	{
		out << "n" << arg << " [label=\"/\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << endl;
	}

	virtual void VisitMultExpression( CMultExpression *arg)
	{
		out << "n" << arg << " [label=\"*\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << endl;
	}

	virtual void VisitSubExpression( CSubExpression *arg)
	{
		out << "n" << arg << " [label=\"-\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << endl;
	}

	virtual void VisitAddExpression( CAddExpression *arg)
	{
		out << "n" << arg << " [label=\"+\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << endl;
	}

	virtual void VisitLessThanExpression( CLessThanExpression *arg )
	{
		out << "n" << arg << " [label=\"<\"];" << endl;
		
		out << "n" << arg << " -> n" << arg->GetLeft() << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << endl;
	}

	virtual void VisitGreaterThanExpression( CGreaterThanExpression *arg )
	{
		out << "n" << arg << " [label=\">\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << ";" << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << ";" << endl;
	}
	
	virtual void VisitLessThanOrEqualToExpression( CLessThanOrEqualToExpression *arg )
	{
		out << "n" << arg << " [label=\"<=\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << ";" << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << ";" << endl;
	}
	
	virtual void VisitGreaterThanOrEqualToExpression( CGreaterThanOrEqualToExpression *arg )
	{
		out << "n" << arg << " [label=\">=\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << ";" << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << ";" << endl;
	}
	
	virtual void VisitEqualToExpression( CEqualToExpression *arg )
	{
		out << "n" << arg << " [label=\"==\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << ";" << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << ";" << endl;
	}
	
	virtual void VisitNotEqualToExpression( CNotEqualToExpression *arg )
	{
		out << "n" << arg << " [label=\"!=\"];" << endl;

		out << "n" << arg << " -> n" << arg->GetLeft() << ";" << endl;
		out << "n" << arg << " -> n" << arg->GetRight() << ";" << endl;
	}

	virtual void VisitStatementBlock( CStatementBlock *arg )
	{
		out << "n" << arg << " [label=\"{...}\"];" << endl;
		CStatementList *stmts = arg->GetStatements();
		for (CStatementList::iterator it = stmts->begin(); it != stmts->end(); it++) {
			out << "n" << arg << " -> n" << (*it) << ";" << endl;
			(*it)->AcceptVisitor(this);
		}
	}

	virtual void VisitIfBlock( CIfBlock *arg )
	{
		out << "n" << arg << " [label=\"if\"];" << endl;

		CExpression *expr = arg->GetConditional();
		CStatement *stmt = arg->GetStatement();

		out << "n" << arg << " -> n" << expr << ";" << endl;
		expr->AcceptVisitor(this);
		
		out << "n" << arg << " -> n" << stmt << ";" << endl;
		stmt->AcceptVisitor(this);
	}

	virtual void VisitWhileBlock( CWhileBlock *arg )
	{
		out << "n" << arg << " [label=\"while\"];" << endl;

		CExpression *expr = arg->GetConditional();
		CStatement *stmt = arg->GetStatement();

		out << "n" << arg << " -> n" << expr << ";" << endl;
		expr->AcceptVisitor(this);
		
		out << "n" << arg << " -> n" << stmt << ";" << endl;
		stmt->AcceptVisitor(this);
	}

	virtual void VisitEmptyStatement( CEmptyStatement *stmt)
	{
		out << "n" << stmt << " [label=\"(removed)\"];" << endl;
	}

};

#endif
