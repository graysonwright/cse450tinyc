#ifndef OPTIMIZE_VISITOR_H
#define OPTIMIZE_VISITOR_H

#include "../Visitor.h"
#include <typeinfo>

class OptimizeVisitor : public CNodeVisitor
{
private:
	std::map<std::string, int> m_constants;

public:
	virtual void VisitProgram( CProgram *prog)
	{
		vector<CDeclaration *> children = prog->GetDeclarations();
		vector<CDeclaration *>::iterator it;
		for(it = children.begin(); it != children.end(); it++)
			(*it)->AcceptVisitor(this);
	}

	virtual void VisitAssignmentOperator( CAssignmentOperator *op )
	{
		op->GetRight()->AcceptVisitor(this);

		// If a statement such as x = x; arises, the statement is deleted.
		if( isVar(op->GetRight()) )
		{
			CExpression* left = op->GetLeft();
			CExpression* right = op->GetRight();
			std::string leftName = ((CVariable *)left)->getName();
			std::string rightName = ((CVariable *) right)->getName();
			if(leftName == rightName)
			{
				if(op->GetParent())
					op->GetParent()->Swap(op, new CEmptyStatement());
			}

		}

		if( isConstant(op->GetRight()) )
		{
			std::string name = op->GetLeft()->getName();
			int value = ((CNumber*) op->GetRight())->getValue();
			m_constants[name] = value;
			// This can cause bug if a variable is used after a while loop
			//if(op->GetParent())
				//op->GetParent()->Swap(op, new CEmptyStatement());
		}
		else
			m_constants.erase(op->GetLeft()->getName());
	}

	virtual void VisitReturnStatement( CReturnStatement *stmt)
	{
		stmt->GetChild()->AcceptVisitor(this);
	}

	virtual void VisitNumber( CNumber *arg) {}
	virtual void VisitVariable( CVariable *arg)
	{
		if(m_constants.find(arg->getName()) != m_constants.end())
			if(arg->GetParent())
				arg->GetParent()->Swap(arg, new CNumber(m_constants[arg->getName()]));
	}

	virtual void VisitWriteStatement( CWriteStatement *arg)
	{
		arg->GetChild()->AcceptVisitor(this);
	}

	virtual void VisitFunctionCall( CFunctionCall *arg)
	{
		vector<CExpression *> children = arg->GetParameters();
		vector<CExpression *>::iterator it;
		for(it = children.begin(); it != children.end(); it++)
			(*it)->AcceptVisitor(this);
	}

	virtual void VisitFuncDeclaration( CFuncDeclaration *arg)
	{
		m_constants.clear();

		CParamDeclarationList &paramDeclarations = arg->GetParamDeclarations();
		CVarDeclarationList &varDeclarations = arg->GetVarDeclarations();
		CStatementList &statements = arg->GetStatements();

		vector<CParamDeclaration *>::iterator itParam;
		vector<CVarDeclaration *>::iterator itVar;
		vector<CStatement *>::iterator itStatement;

		for(itParam = paramDeclarations.begin(); itParam != paramDeclarations.end(); itParam++)
			(*itParam)->AcceptVisitor(this);

		for(itVar = varDeclarations.begin(); itVar != varDeclarations.end(); itVar++)
			(*itVar)->AcceptVisitor(this);

		for(itStatement = statements.begin(); itStatement != statements.end(); itStatement++)
			(*itStatement)->AcceptVisitor(this);

		m_constants.clear();
	}

	// No optimizations possible at this time.
	// Have context keep track of which variables are constants?
	virtual void VisitParamDeclaration( CParamDeclaration *arg) {}
	virtual void VisitVarDeclaration( CVarDeclaration *arg) {}

	virtual void VisitModExpression( CModExpression *arg)
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
		{
			int left = ((CNumber *)arg->GetLeft())->getValue();
			int right = ((CNumber *)arg->GetRight())->getValue();

			CNumber *newNode = new CNumber(left % right);
			// swap out newNode for arg
			if(arg->GetParent())
				arg->GetParent()->Swap(arg, newNode);
		}
	}

	virtual void VisitDivExpression( CDivExpression *arg)
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
		{
			int left = ((CNumber *)arg->GetLeft())->getValue();
			int right = ((CNumber *)arg->GetRight())->getValue();

			CNumber *newNode = new CNumber(left / right);
			// swap out newNode for arg
			if(arg->GetParent())
				arg->GetParent()->Swap(arg, newNode);
		}
	}

	virtual void VisitMultExpression( CMultExpression *arg)
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		// SHIFT IMPLEMENTATION!!!!!
		// swap out newNode for arg (TEMPORARY)
		//if(arg->GetParent())
			//arg->GetParent()->Swap(arg, new CShiftExpression(arg->GetLeft(), new CNumber(3)));

		
		if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
		{
			int left = ((CNumber *)arg->GetLeft())->getValue();
			int right = ((CNumber *)arg->GetRight())->getValue();

			CNumber *newNode = new CNumber(left * right);
			// swap out newNode for arg
			if(arg->GetParent())
				arg->GetParent()->Swap(arg, newNode);
		}

		// Check two things:
		// Checks to see if the left child is a constant 0, changes expression to CNumber(0)
		// Checks to see if the left child is a constant 1, changes expression to only the right expression
		else if(isConstant(arg->GetLeft()))
		{
			int left = ((CNumber *)arg->GetLeft())->getValue();
			if(left == 0)
			{
				CNumber *newNode = new CNumber(0);
				// swap out newNode for arg
				if(arg->GetParent())
					arg->GetParent()->Swap(arg, newNode);
			}
			else if (left == 1)
			{
				// swap out newNode for arg
				if(arg->GetParent())
					arg->GetParent()->Swap(arg, arg->GetRight());
			}
		}

		// Checks two things:
		// Checks to see if the right child is a constant 0, changes expression to CNumber(0)
		// Checks to see if the right child is a constant 1, changes expression to only the left expression
		else if(isConstant(arg->GetRight()))
		{
			int right = ((CNumber *)arg->GetRight())->getValue();
			if(right == 0)
			{
				CNumber *newNode = new CNumber(0);
				// swap out newNode for arg
				if(arg->GetParent())
					arg->GetParent()->Swap(arg, newNode);
			} 
			else if (right == 1)
			{
				// swap out newNode for arg
				if(arg->GetParent())
					arg->GetParent()->Swap(arg, arg->GetLeft());
			}
		}
	}

	virtual void VisitSubExpression( CSubExpression *arg)
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
		{
			int left = ((CNumber *)arg->GetLeft())->getValue();
			int right = ((CNumber *)arg->GetRight())->getValue();

			CNumber *newNode = new CNumber(left - right);
			// swap out newNode for arg
			if(arg->GetParent())
				arg->GetParent()->Swap(arg, newNode);
		}
		// Checks to see if the left child is a constant 0, changes expression to only the right expression
		else if(isConstant(arg->GetLeft()))
		{
			int left = ((CNumber *)arg->GetLeft())->getValue();
			if(left == 0)
			{
				// swap out newNode for arg
				if(arg->GetParent())
					arg->GetParent()->Swap(arg, arg->GetRight());
			}
		}
		// Checks to see if the right child is a constant 0, changes expression to only the left expression
		else if(isConstant(arg->GetRight()))
		{
			int right = ((CNumber *)arg->GetRight())->getValue();
			if (right == 0)
			{
				// swap out newNode for arg
				if(arg->GetParent())
					arg->GetParent()->Swap(arg, arg->GetLeft());
			}
		}
	}

	virtual void VisitAddExpression( CAddExpression *arg)
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
		{
			int left = ((CNumber *)arg->GetLeft())->getValue();
			int right = ((CNumber *)arg->GetRight())->getValue();

			CNumber *newNode = new CNumber(left + right);
			// swap out newNode for arg
			if(arg->GetParent())
				arg->GetParent()->Swap(arg, newNode);
		}

		// Checks to see if the left child is a constant 0, changes expression to only the right expression
		else if(isConstant(arg->GetLeft()))
		{
			int left = ((CNumber *)arg->GetLeft())->getValue();
			if(left == 0)
			{
				// swap out newNode for arg
				if(arg->GetParent())
					arg->GetParent()->Swap(arg, arg->GetRight());
			}
		}
		// Checks to see if the right child is a constant 0, changes expression to only the left expression
		else if(isConstant(arg->GetRight()))
		{
			int right = ((CNumber *)arg->GetRight())->getValue();
			if (right == 0)
			{
				// swap out newNode for arg
				if(arg->GetParent())
					arg->GetParent()->Swap(arg, arg->GetLeft());
			}
		}
	}

	virtual void VisitLessThanExpression( CLessThanExpression *arg )
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(arg->GetParent())
			if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
			{
				if( ((CNumber*)arg->GetLeft())->getValue() < ((CNumber*)arg->GetRight())->getValue() ) 
					arg->GetParent()->Swap(arg, new CNumber(1));
				else
					arg->GetParent()->Swap(arg, new CNumber(0));
			}
	}

	virtual void VisitGreaterThanExpression( CGreaterThanExpression *arg )
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(arg->GetParent())
			if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
			{
				if( ((CNumber*)arg->GetLeft())->getValue() > ((CNumber*)arg->GetRight())->getValue() ) 
					arg->GetParent()->Swap(arg, new CNumber(1));
				else
					arg->GetParent()->Swap(arg, new CNumber(0));
			}
	}
	
	virtual void VisitLessThanOrEqualToExpression( CLessThanOrEqualToExpression *arg )
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(arg->GetParent())
			if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
			{
				if( ((CNumber*)arg->GetLeft())->getValue() <= ((CNumber*)arg->GetRight())->getValue() ) 
					arg->GetParent()->Swap(arg, new CNumber(1));
				else
					arg->GetParent()->Swap(arg, new CNumber(0));
			}
	}
	
	virtual void VisitGreaterThanOrEqualToExpression( CGreaterThanOrEqualToExpression *arg )
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(arg->GetParent())
			if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
			{
				if( ((CNumber*)arg->GetLeft())->getValue() >= ((CNumber*)arg->GetRight())->getValue() ) 
					arg->GetParent()->Swap(arg, new CNumber(1));
				else
					arg->GetParent()->Swap(arg, new CNumber(0));
			}
	}
	
	virtual void VisitEqualToExpression( CEqualToExpression *arg )
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(arg->GetParent())
			if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
			{
				if( ((CNumber*)arg->GetLeft())->getValue() == ((CNumber*)arg->GetRight())->getValue() ) 
					arg->GetParent()->Swap(arg, new CNumber(1));
				else
					arg->GetParent()->Swap(arg, new CNumber(0));
			}
	}
	
	virtual void VisitNotEqualToExpression( CNotEqualToExpression *arg )
	{
		arg->GetLeft()->AcceptVisitor(this);
		arg->GetRight()->AcceptVisitor(this);

		if(isConstant(arg->GetLeft()) && isConstant(arg->GetRight()))
		{
			if( ((CNumber*)arg->GetLeft())->getValue() != ((CNumber*)arg->GetRight())->getValue() ) 
			{
				if(arg->GetParent())
					arg->GetParent()->Swap(arg, new CNumber(1));
			}
		}
	}

	virtual void VisitStatementBlock( CStatementBlock *arg )
	{

		/*CStatementList *stmts= arg->GetStatements();
		for(CStatementList::iterator it = stmts->begin(); it != stmts->end(); it++)
		{
			(*it)->AcceptVisitor(this);
		}*/
		
	}

	virtual void VisitIfBlock( CIfBlock *arg )
	{
		arg->GetConditional()->AcceptVisitor(this);

		if(arg->GetParent())
			if(isConstant(arg->GetConditional()))
			{
				if( ((CNumber*)arg->GetConditional())->getValue() == 0)
					arg->GetParent()->Swap(arg, new CEmptyStatement());
				else 
					arg->GetParent()->Swap(arg, arg->GetStatement());
				return;
			}
		
		m_constants.clear();
	}

	virtual void VisitWhileBlock( CWhileBlock *arg )
	{
		m_constants.clear();

		arg->GetConditional()->AcceptVisitor(this);

		/*if(isConstant(arg->GetConditional()))
		{
			if( ((CNumber*)arg->GetConditional())->getValue() == 0)
			{
				arg->GetParent()->Swap(arg, new CEmptyStatement());
			} 

			else 
			{
				arg->GetParent()->Swap(arg, arg->GetStatement());
			}

		}*/
		m_constants.clear();
	}

private:
	bool isConstant(CExpression *expr)
	{
		CNumber x(4);
		return (typeid(x) == typeid(*expr));
	}

	bool isVar(CExpression *expr)
	{
		CVariable x("VARIABLE_NAME");
		return (typeid(x) == typeid(*expr));
	}

	virtual void VisitEmptyStatement(CEmptyStatement *stmt) {}

};

#endif
