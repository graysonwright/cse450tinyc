#ifndef X86_VISITOR_H
#define X86_VISITOR_H

#include "../Visitor.h"
#include "../Assignment.h"

class X86Visitor : public CNodeVisitor
{
private:
	CContext *m_context;
	ostream &m_out;

public:
  
  X86Visitor(ostream &out): m_out(out) { m_context = NULL; }

	virtual void VisitProgram( CProgram *prog)
	{
		m_out << ".section .data" << endl;
		m_out << endl;

		m_out << ".section .text" << endl;
		m_out << ".globl _start" << endl;
		m_out << endl;
		m_out << "_start:" << endl;
		m_out << "\tcall main" << endl;
		m_out << "\tjmp exit" << endl;
		m_out << ".include \"./x86asm/print_int.s\"" << endl;

		// Print variable and function declarations
		vector<CDeclaration *> decs = prog->GetDeclarations();
		for(unsigned int i = 0; i < decs.size(); i++)
			decs[i]->AcceptVisitor(this);

		m_out << ".type exit, @function" << endl;
		m_out << "exit:" << endl;
		m_out << "\tmovl $0, %ebx" << endl;
		m_out << "\tmovl $1, %eax" << endl;
		m_out << "\tint $0x80" << endl;

	}

	virtual void VisitAssignmentOperator( CAssignmentOperator *op )
	{
		op->GetRight()->AcceptVisitor(this);
		string id = op->GetLeft()->getName();
		m_out << "\tpopl " << m_context->getLocation(id) << "(%ebp)";
		m_out << "\t\t/* Assignment to " << id << " */" << endl;
	}

	virtual void VisitReturnStatement( CReturnStatement *stmt)
	{
		stmt->GetChild()->AcceptVisitor(this);
	}

	virtual void VisitNumber( CNumber *arg)
	{
		m_out << "\tpushl $" << arg->getValue() << "\t\t/* Pushing constant onto stack */" << endl;
	}

	virtual void VisitVariable( CVariable *arg)
	{
		m_out << "\tpushl " << m_context->getLocation(arg->getName()) << "(%ebp)";
		m_out << "\t\t/* Push variable " << arg->getName() << " onto the stack */" << endl;
	}

	virtual void VisitWriteStatement( CWriteStatement *arg)
	{
		arg->GetChild()->AcceptVisitor(this);
		m_out << "\tcall print_int" << endl;
	}

	virtual void VisitFunctionCall( CFunctionCall *arg)
	{
		CParameterList &params = arg->GetParameters();

		for(unsigned int i = 0; i < params.size(); i++)
		{
			CExpression* exp = params[i];
			exp->AcceptVisitor(this);
		}
		m_out << "\tcall " << arg->GetName() << "\t/* Call " << arg->GetName() << " */" << endl; 
		m_out << "\tpushl %eax \t\t/* Pushing the return value to register %eax */" << endl;
	}

	virtual void VisitFuncDeclaration( CFuncDeclaration *arg)
	{
		m_context = new CContext();
		// Set up variables
    
		CParamDeclarationList &paramDeclarations = arg->GetParamDeclarations();
		CVarDeclarationList &varDeclarations = arg->GetVarDeclarations();
		CStatementList &statements = arg->GetStatements();
    
		for(unsigned int i = 0; i < paramDeclarations.size(); i++)
		{
			CVariable* var = new CVariable(paramDeclarations[i]->getName());
			m_context->addVariable(var->getName(), 8 + (4 * (paramDeclarations.size() - i-1)));
		}
    
		for(unsigned int i = 0; i < varDeclarations.size(); i++)
		{
			CVariable* var = new CVariable(varDeclarations[i]->getName());
			m_context->addVariable(var->getName(), -4*(i+1));
		}
    
    // ---------
    
		m_out << endl;
		m_out << "/* ----- Set up ----- */" << endl;
		m_out << ".type " << arg->getName() << ", @function" << endl;
		m_out << ".globl " << arg->getName() << endl;
		m_out << arg->getName() << ":" << endl;
		m_out << "\tpushl %ebp" << "/* save base (frame) pointer on stack */" << endl;
		m_out << "\tmovl %esp, %ebp" << "/* base pointer is stack pointer */" << endl;
		m_out << endl;

		m_out << endl;
		m_out << "/* ----- Body ----- */" << endl;

		for(unsigned int i = 0; i < varDeclarations.size(); i++)
			varDeclarations[i]->AcceptVisitor(this);

		for(unsigned int i = 0; i < statements.size(); i++)
			statements[i]->AcceptVisitor(this);
		
		m_out << endl;
		m_out << "/* ----- Finish ----- */" << endl;
		m_out << "\tpopl %eax" << endl;
		m_out << "\tmovl %ebp, %esp" << endl;
		m_out << "\tpopl %ebp" << endl;
		m_out << "\tret" << endl;
    
		delete m_context;
		m_context = NULL;
	}	

	/* TODO: ...? */
	virtual void VisitParamDeclaration( CParamDeclaration *arg) {}

	virtual void VisitVarDeclaration( CVarDeclaration *arg)
	{
		m_out << "\taddl $-4, %esp";
		m_out << "\t/* Making space on the stack for " << arg->getName() << "*/" << endl;
	}
	
	virtual void VisitModExpression( CModExpression *arg)
	{
		m_out << "\tpopl %ebx" << endl;
		m_out << "\tpopl %eax" << endl;
		m_out << "\tcltd" << endl;
		m_out << "\tpush %ebx" << endl;
		m_out << "\tidivl (%esp)" << "\t\t/* DIVIDE */" << endl;
		m_out << "\tmovl %edx, (%esp)" << endl;
	}
		
	virtual void VisitDivExpression( CDivExpression *arg)
	{
		m_out << "\tpopl %ebx" << endl;
		m_out << "\tpopl %eax" << endl;
		m_out << "\tcltd" << endl;
		m_out << "\tpush %ebx" << endl;
		m_out << "\tidivl (%esp)" << "\t\t/* DIVIDE */" << endl;
		m_out << "\tmovl %eax, (%esp)" << endl;
	}
	
	virtual void VisitMultExpression( CMultExpression *arg)
	{
		m_out << "\tpopl %eax" << endl;
		m_out << "\tcltd" << endl;
		m_out << "\timull (%esp)" << "\t\t/* MULTIPLY */" << endl;
		m_out << "\tmovl %eax, (%esp)" << endl;
	}

	virtual void VisitSubExpression( CSubExpression *arg)
	{
		m_out << "\tpopl %eax" << endl;
		m_out << "\tsubl %eax, (%esp)" << "\t\t/* SUBTRACT */" << endl;
	}

	virtual void VisitAddExpression( CAddExpression *arg)
	{
		m_out << "\tpopl %eax" << endl;
		m_out << "\taddl %eax, (%esp)" << "\t\t/* ADD */" << endl;
	}

	virtual void VisitLessThanExpression( CLessThanExpression *arg )
	{
		m_out << "\tpopl %eax" << endl;
		m_out << "\tcmpl %eax, (%esp)" << endl;
		m_out << "\tjl yes" << arg << "\t\t/* LESSTHAN */" << endl;
		m_out << "\tmovl $0, (%esp)" << endl;		//Logic if expression is false
		m_out << "\tjmp done" << arg << endl;
		m_out << "yes" << arg << ":" << endl;
		m_out << "\tmovl $1, (%esp)" << endl;		//Logic if expression is true
		m_out << "done" << arg << ":" << endl;
	}

	virtual void VisitGreaterThanExpression( CGreaterThanExpression *arg )
	{
		m_out << "\tpopl %eax" << endl;
		m_out << "\tcmpl %eax, (%esp)" << endl;
		m_out << "\tjg yes" << arg << "\t\t/* GREATERTHAN */" << endl;
		m_out << "\tmovl $0, (%esp)" << endl;
		m_out << "\tjmp done" << arg << endl;
		m_out << "yes" << arg << ":" << endl;
		m_out << "\tmovl $1, (%esp)" << endl;
		m_out << "done" << arg << ":" << endl;
	}
	
	virtual void VisitLessThanOrEqualToExpression( CLessThanOrEqualToExpression *arg )
	{
		m_out << "\tpopl %eax" << endl;
		m_out << "\tcmpl %eax, (%esp)" << endl;
		m_out << "\tjle yes" << arg << "\t\t/* LESSTHANOREQUALTO */" << endl;
		m_out << "\tmovl $0, (%esp)" << endl;
		m_out << "\tjmp done" << arg << endl;
		m_out << "yes" << arg << ":" << endl;
		m_out << "\tmovl $1, (%esp)" << endl;
		m_out << "done" << arg << ":" << endl;
	}
	
	virtual void VisitGreaterThanOrEqualToExpression( CGreaterThanOrEqualToExpression *arg )
	{
		m_out << "\tpopl %eax" << endl;
		m_out << "\tcmpl %eax, (%esp)" << endl;
		m_out << "\tjge yes" << arg << "\t\t/* GREATERTHANOREQUALTO */" << endl;
		m_out << "\tmovl $0, (%esp)" << endl;
		m_out << "\tjmp done" << arg << endl;
		m_out << "yes" << arg << ":" << endl;
		m_out << "\tmovl $1, (%esp)" << endl;
		m_out << "done" << arg << ":" << endl;
	}
	
	virtual void VisitEqualToExpression( CEqualToExpression *arg )
	{
		m_out << "\tpopl %eax" << endl;
		m_out << "\tcmpl %eax, (%esp)" << endl;
		m_out << "\tje yes" << arg << "\t\t/* EQUALTO */" << endl;
		m_out << "\tmovl $0, (%esp)" << endl;
		m_out << "\tjmp done" << arg << endl;
		m_out << "yes" << arg << ":" << endl;
		m_out << "\tmovl $1, (%esp)" << endl;
		m_out << "done" << arg << ":" << endl;
	}
	
	virtual void VisitNotEqualToExpression( CNotEqualToExpression *arg )
	{
		m_out << "\tpopl %eax" << endl;
		m_out << "\tcmpl %eax, (%esp)" << endl;
		m_out << "\tjne yes" << arg << "\t\t/* NOTEQUALTO */" << endl;
		m_out << "\tmovl $0, (%esp)" << endl;
		m_out << "\tjmp done" << arg << endl;
		m_out << "yes" << arg << ":" << endl;
		m_out << "\tmovl $1, (%esp)" << endl;
		m_out << "done" << arg << ":" << endl;
	}

	
	virtual void VisitIfBlock( CIfBlock *arg)
	{
		arg->GetConditional()->AcceptVisitor(this);
		m_out << "\tcmpl $0, (%esp)" << "\t\t/* IF STATEMENT */" << endl;
		m_out << "\tje after" << arg << endl;
		arg->GetStatement()->AcceptVisitor(this);
		m_out << "after" << arg << ":" << endl;
	}
	
	virtual void VisitWhileBlock( CWhileBlock *arg )
	{
		m_out << "start" << arg << ":" << endl;
		arg->GetConditional()->AcceptVisitor(this);
		m_out << "\tcmpl $0, (%esp)" << endl;
		m_out << "\tje end" << arg << endl;
		arg->GetStatement()->AcceptVisitor(this);
		m_out << "\tjmp start" << arg << endl;
		m_out << "end" << arg << ":" << endl;
	}

	virtual void VisitStatementBlock( CStatementBlock *arg ) 
	{
		CStatementList *stmts= arg->GetStatements();
		for(CStatementList::iterator it = stmts->begin(); it != stmts->end(); it++)
		{
			(*it)->AcceptVisitor(this);
		}
	}

	virtual void VisitEmptyStatement(CEmptyStatement *stmt) {}

};

#endif
