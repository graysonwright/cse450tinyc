%{
  #include <iostream>
  #include <string>
  #include "includes.h"

  // Bring the standard library into the global namespace
  using namespace std;

  // Prototypes to keep the compiler happy
  void yyerror (const char *error);
  int  yylex ();
  
  bool optimizeFlag = true;
%}

%union {
  int			value;
  CDeclaration		*declaration;
  CProgram		*program;
  string		*str;
  CExpression		*expr;
  CStatement		*stmt;
  CStatementList	*stmtlist;
  CFuncDeclaration	*func_dec;
  CVarDeclaration	*var_dec;
  CVarDeclarationList	*var_dec_list;
  CVariable		*var;
  CParameterList	*params;
  CParamDeclaration	*param_dec;
  CParamDeclarationList *param_dec_list;

  CConditional		*conditional;
  CIfBlock		*if_block;
  CWhileBlock		*while_block;
  CStatementBlock	*statement_block;
  /* CForBlock		*for_block; */
}

%token <value> NUMBER
%token INT WRITE ASSIGN RETURN DOT LESSOR GREATEROR EQUALTO NOTEQUALTO IF ELSE WHILE FOR
%token <str> NAME

%type <declaration> declaration
%type <func_dec> func_declaration
%type <var_dec> var_declaration
%type <var_dec_list> var_declarations 
%type <program> program
%type <expr> exp
%type <var> var
%type <params> pars
%type <param_dec> formal_par
%type <param_dec_list> formal_pars
%type <stmt> statement /* ifblock whileblock forblock */
%type <stmtlist> statements
%type <statement_block> block
%type <conditional> conditional

%left '<' '>' LESSOR GREATEROR EQUALTO NOTEQUALTO
%left '+' '-'
%left '%'
%left '*' '/'
%left PAREN

%%

 /* ------------------ Parser Productions ------------------ */

end: program	{
			if(optimizeFlag) $1->AcceptVisitor(&OptimizeVisitor());
			 X86Visitor v(cout); $1->AcceptVisitor(&v);
		}
   ;

program: declaration					{ $$ = new CProgram(); $$->addDeclaration($1); }
       | declaration program 				{ $$ = $2; $$->addDeclaration($1); }
       ;

declaration: func_declaration				{ $$ = $1; }
	   | var_declaration				{ $$ = $1; }
	   | DOT NAME declaration                       { $$ = $3; DotVisitor v((*$2)+ ".dot"); $$->AcceptVisitor(&v);
         						  $$->AcceptVisitor(&OptimizeVisitor());
         						  DotVisitor v2((*$2)+"-optimized.dot"); $$->AcceptVisitor(&v2); }
	   ;

func_declaration: INT NAME  '(' formal_pars ')' '{' var_declarations statements '}'	{ $$ = new CFuncDeclaration($4, $7, $8); $$->setName(*$2); }
		;


formal_pars: formal_par					{ $$ = new CParamDeclarationList($1); }
	   | formal_pars ',' formal_par			{ $$ = $1; $$->push_back($3); }
	   | 						{ $$ = new CParamDeclarationList(); }
	   ;

formal_par: INT NAME					{  $$ = new CParamDeclaration(); $$->setName(*$2);}
	  ;

var_declarations: var_declarations var_declaration	{ $$ = $1; $$->push_back($2); }
		| 				 	{ $$ = new CVarDeclarationList(); }
		;

var_declaration: INT NAME ';'				{ $$ = new CVarDeclaration(); $$->setName(*$2); }
	       | DOT NAME var_declaration		{ $$ = $3; DotVisitor v((*$2)+ ".dot"); $$->AcceptVisitor(&v);
         						  $$->AcceptVisitor(&OptimizeVisitor());
         						  DotVisitor v2((*$2)+"-optimized.dot"); $$->AcceptVisitor(&v2); }
	       ;

statements: statement ';'				{ $$ = new CStatementList($1); }
	  | statements statement ';'			{ $$ = $1; $$->push_back($2); }
	  ;

statement: var ASSIGN exp				{ $$ = new CAssignmentOperator($1, $3); }
	 | RETURN exp					{ $$ = new CReturnStatement($2); }
	 | NAME '(' pars ')'				{ $$ = new CFunctionCall(*$1, $3); }
	 | WRITE exp					{ $$ = new CWriteStatement($2); }
         | DOT NAME statement                           { $$ = $3; DotVisitor v((*$2)+ ".dot"); $$->AcceptVisitor(&v);
         						  $$->AcceptVisitor(&OptimizeVisitor());
         						  DotVisitor v2((*$2)+"-optimized.dot"); $$->AcceptVisitor(&v2); }
         | conditional					{ $$ = (CStatement*) $1; }
	 ;

block: '{' statements '}'				{ $$ = new CStatementBlock($2); }
     ;

exp: exp '+' exp					{ $$ = new CAddExpression($1, $3); }
   | exp '-' exp					{ $$ = new CSubExpression($1, $3); }
   | exp '*' exp					{ $$ = new CMultExpression($1, $3); }
   | exp '/' exp					{ $$ = new CDivExpression($1, $3); }
   | exp '%' exp					{ $$ = new CModExpression($1, $3); }
   | NAME '(' pars ')'					{ $$ = new CFunctionCall(*$1, $3); }
   | '(' exp ')' %prec PAREN				{ $$ = $2; }
   | NUMBER						{ $$ = new CNumber($1); }
   | var						{}
   | '-' NUMBER						{ $$ = new CNumber(-$2); }
 /* ---- Conditional Business ---- */
   | exp '<' exp					{ $$ = new CLessThanExpression($1, $3); }
   | exp '>' exp					{ $$ = new CGreaterThanExpression($1, $3); }
   | exp LESSOR exp					{ $$ = new CLessThanOrEqualToExpression($1, $3); }
   | exp GREATEROR exp					{ $$ = new CGreaterThanOrEqualToExpression($1, $3); }
   | exp EQUALTO exp					{ $$ = new CEqualToExpression($1, $3); }
   | exp NOTEQUALTO exp					{ $$ = new CNotEqualToExpression($1, $3); }
   ;

pars: exp						{ $$ = new CParameterList($1);}
    | pars ',' exp					{ $$ = $1; $$->push_back($3); }
    | 							{ $$ = new CParameterList(); }
    ;

var: NAME						{ $$ = new CVariable( $1 ); }
   ;

 /* ----- Conditional Branching ----- */

conditional: WHILE '(' exp ')' block			{ $$ = new CWhileBlock( $3, $5 );}
           | IF '(' exp ')' block			{ $$ = new CIfBlock( $3, $5 ); }
           ;


%%

int main (int argc, char *argv[])
{
  return yyparse ();
}

void yyerror (const char *error)
{
  cout << error << endl;
}
