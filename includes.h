#ifndef INCLUDES_H
#define INCLUDES_H

#include "classes/Node.h"
#include "classes/Statement.h"
#include "classes/Expression.h"
#include "classes/Program.h"

#include "classes/Declarations.h"
#include "classes/BinaryOperators.h"
#include "classes/Assignment.h"
#include "classes/Output.h"
#include "classes/Atoms.h"
#include "classes/Variables.h"
#include "classes/Conditionals.h"

#include "classes/visitors/X86Visitor.h"
#include "classes/visitors/OptimizeVisitor.h"
#include "classes/visitors/DotVisitor.h"

#endif
