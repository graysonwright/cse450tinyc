all: clean tinyc tests

## CREATE EXECUTABLE
tinyc: grammar.tab.c lex.yy.c
	g++ -Wall grammar.tab.c lex.yy.c -o tinyc

## Compile Parser
grammar.tab.c grammar.tab.h: grammar.y includes.h classes/*
	bison --defines grammar.y

## Compile Lexer
lex.yy.c: patterns.l grammar.tab.h
	flex patterns.l

tests: tinyc
	./runtests

clean:
	rm -rf tinyc grammar.tab.c grammar.tab.h lex.yy.c *.o *.s t? demo *.dot bin exe out
