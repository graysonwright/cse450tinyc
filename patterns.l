%{

#include <string>
#include <iostream>
#include "includes.h"
#include "grammar.tab.h"

using namespace std;
%}

%s MULTICOMMENT STRING

%%

 /* Single-Line Comments */
"//".*							{}

 /* Multi-Line Comments */
<INITIAL>"/*"						{ BEGIN(MULTICOMMENT); }
<MULTICOMMENT>.|\n					{}
<MULTICOMMENT>"*/"					{ BEGIN(INITIAL); }

 /* ----- Whitespace ----- */
<INITIAL>[ \t\r\n]* 					/* Skip whitespace */ {}

 /* ----- Keywords ----- */
<INITIAL>"int"						{ return INT; }
<INITIAL>"return"					{ return RETURN; }
<INITIAL>"write"					{ return WRITE; }
<INITIAL>"="						{ return ASSIGN; }

<INITIAL>"if"						{ return IF; }
<INITIAL>"else"						{ return ELSE; }
<INITIAL>"while"					{ return WHILE; }
<INITIAL>"for"						{ return FOR; }

 /* ----- Operators ----- */
<INITIAL>"<="						{ return LESSOR; }
<INITIAL>">="						{ return GREATEROR; }
<INITIAL>"=="						{ return EQUALTO; }
<INITIAL>"!="						{ return NOTEQUALTO; }

 /* ----- Names and numbers ----- */
<INITIAL>[_a-zA-Z][_a-zA-Z0-9]*				{ yylval.str = new string(yytext); return NAME; }
<INITIAL>[0-9]+						{ yylval.value = atoi(yytext); return NUMBER; }

<INITIAL>@DOT                                           { return DOT; }

<INITIAL>.	 					{ return yytext[0]; }

%%

int yywrap()
{
	return 1;
}
